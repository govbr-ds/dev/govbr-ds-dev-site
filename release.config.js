const { branches, commitAnalyzer, releaseNotesGenerator, changelog, git, gitlab } = require('@govbr-ds/release-config')

module.exports = {
	branches: branches,
	plugins: [commitAnalyzer, releaseNotesGenerator, changelog, gitlab, git],
}
