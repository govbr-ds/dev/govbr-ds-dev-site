// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
	production: false,
	debugMode: true,
	baseUrl: 'https://docs-ds.estaleiro.serpro.gov.br/',
	docsUrl: 'docs',
	designDocUrl: 'https://docs-ds.estaleiro.serpro.gov.br/govbr-ds/ds/',
	urlCore: 'https://docs-ds.estaleiro.serpro.gov.br/govbr-ds-core/',
	docUrl: 'https://docs-ds.estaleiro.serpro.gov.br/govbr-ds-core/docs/',
	staticContentUrl: 'https://docs-ds.estaleiro.serpro.gov.br/govbr-ds-site-content/',
	corecssUrl: 'https://docs-ds.estaleiro.serpro.gov.br/govbr-ds-core/dist/core.css',
	coreinitJstUrl: 'https://docs-ds.estaleiro.serpro.gov.br/govbr-ds-core/dist/core-init.js',
	YOUR_GTM_ID: 'GTM-NPF649K',

	HJID: '2360609',
}

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
import 'zone.js/plugins/zone-error' // Included with Angular CLI.
