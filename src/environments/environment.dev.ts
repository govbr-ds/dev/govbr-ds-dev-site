// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
	production: true,
	debugMode: false,
	baseUrl: 'http://localhost:9001/',
	docsUrl: 'docs',
	designDocUrl: 'http://localhost:9001/govbr-ds/ds/',
	urlCore: 'http://localhost:9001/govbr-ds-core/',
	docUrl: 'http://localhost:9001/govbr-ds-core/docs/',
	staticContentUrl: 'http://localhost:9001/govbr-ds-site-content/',
	corecssUrl: 'http://localhost:9001/govbr-ds-core/dist/core.css',
	coreinitJstUrl: 'http://localhost:9001/govbr-ds-core/dist/core-init.js',
	YOUR_GTM_ID: 'GTM-NPF649K',

	HJID: '2360609',
}

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
import 'zone.js/plugins/zone-error' // Included with Angular CLI.
