# Padrão Digital de Governo - Site

## Informações

Esse projeto é feito usando Angular 8+.

Esse repositório não é o Design System. Caso esteja procurando o Design System procure no mesmo grupo o repositório correspondente.

O Design System é consumido como submódulo git na pasta assets. Assim, sempre que for necessário alguma versão específica do Design System é necessário atualizar o submódulo.

## Tecnologias

Esse projeto é desenvolvido usando:

1. [Angular](https://angular.io/ 'Angular').
1. [SASS](https://sass-lang.com/ 'SASS')

## Dependências

As principais dependências do projeto são:

1. [Design System GOV.BR.BR](https://cdngovbr-ds.estaleiro.serpro.gov.br/ 'Design System')

1. [Font Awesome](https://fontawesome.com/ 'Font Awesome')

1. [Fonte Rawline](https://www.cdnfonts.com/rawline.font/ 'Fonte Rawline')

> O fontawesome e a fonte rawline podem ser importadas de um CDN. Consulte a documentação no site do [Design System](http://cdngovbr-ds.estaleiro.serpro.gov.br/ 'Design System') para mais detalhes.

## Como executar o projeto?

```sh
git clone https://git.serpro.gov.br/govbr-ds/govbr-ds-site.git

git checkout main

npm run init
```

> Se o seu sistema operacional for windows, habilite os caminhos longos para arquivos: [https://helpdeskgeek.com/how-to/how-to-fix-filename-is-too-long-issue-in-windows/](https://helpdeskgeek.com/how-to/how-to-fix-filename-is-too-long-issue-in-windows/)

### Submódulo

Execute os comando abaixo a partir da raiz.

```bash
cd src/assets/govbr-ds-dev-core

git checkout main

npm install

npm run build
```

> Todos os comandos devem ser executados com sucesso para conseguir rodar uma instância do site.

### Rodando o site

```bash
ng run start
```

ou

```bash
ng serve
```

## Resolvendo problemas com o submódulo

<details>
  <summary>Problema para baixar o submódulo</summary>

```bash
git config --global http.sslverify false
```

</details>

<details>
  <summary>Submódulo (src/assets/govbr-ds-dev-core) não atualiza ou pasta está vazia</summary>

```bash
git submodule update --init --recursive
```

</details>

##  Lints

Nesse projeto usamos diversos tipos de lints para automaticamente verificar o código antes de enviar para review. Para executá-los e obter os resultados execute o comando:

```bash
npm run lint
```

## Precisa de ajuda?

> Por favor **não** crie issues para fazer perguntas...

Use nossos canais abaixo para obter tirar suas dúvidas:

- Site do Design System GOV.BR.BR <http://cdngovbr-ds.estaleiro.serpro.gov.br/>

- Web Components (versão estável) <https://govbr-ds.gitlab.io/govbr-ds-webcomponents/main>

- Web Components (versão em desenvolvimento\*) <https://govbr-ds.gitlab.io/govbr-ds-webcomponents/develop>

- Pelo nosso email

- Usando nosso canal no discord <https://discord.gg/NkaVZERAT7>

## Como contribuir?

Por favor verifique nossos guias de [como contribuir](./CONTRIBUTING.md 'Como contribuir?').

### Commits

Nesse projeto usamos um padrão para branches e commits. Por favor observe a documentação na nossa [wiki](https://gov.br/ds/wiki/ 'Wiki') para aprender sobre os nossos padrões.

## Créditos

Os Web Components do Design System são criados pelo [SERPRO](https://www.serpro.gov.br/ 'SERPRO | Serviço Federal de Processamento de Dados') juntamente com a participação da comunidade.

## Licença

Nesse projeto usamos a licença MIT.
