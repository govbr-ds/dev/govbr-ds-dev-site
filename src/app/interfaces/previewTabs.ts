import { IPreviewTab } from './previewTab'

export interface IPreviewTabs {
	label: string
	tabs: Array<IPreviewTab>
	height?: number
}
