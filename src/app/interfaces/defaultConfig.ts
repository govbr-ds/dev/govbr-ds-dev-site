export interface IDefaultConfig {
	forType: string
	ext: string[]
	htmlFilename?: string
	default: boolean
	developer: boolean
	designer: boolean
	accessibility: boolean
}
